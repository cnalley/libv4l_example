/* V4L2 video picture grabber
   Copyright (C) 2009 Mauro Carvalho Chehab <mchehab@infradead.org>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation version 2 of the License.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
 */

/* Good documentation found so far:
 * 
 * http://www.jayrambhia.com/blog/capture-v4l2
 * 
 */
 #include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <linux/videodev2.h>
#include "/usr/include/libv4l2.h"

#define CLEAR(x) memset(&(x), 0, sizeof(x))


#define REQUESTED_BUFFERS           4
#define FRAMES_TO_GRAB              1000
#define FRAMERATE                   30
#define FRAME_WIDTH                 320
#define FRAME_HEIGHT                180
#define DEVICE_NAME                 "/dev/video0"
#define MEMORY_TYPE                 V4L2_MEMORY_USERPTR
#define BUFFER_TYPE                 V4L2_BUF_TYPE_VIDEO_CAPTURE
#define VIDEO_FORMAT                V4L2_PIX_FMT_MJPEG /* Motion JPEG format */ 
#define VIDEO_FIELD_ORDER           V4L2_FIELD_NONE /* Progressive */
#define BUFFER_FILL_TIMEOUT_SEC     2
#define BUFFER_FILL_TIMEOUT_USEC    0

#define SERVER_IP                   "10.10.0.10"
#define SERVER_PORT                 3001


struct buffer {
    void   *start;
    size_t length;
};


struct v4l2_format              fmt;
struct v4l2_buffer              buf;
struct v4l2_requestbuffers      req;
/* Streaming parameters */
struct v4l2_streamparm          parm;
int                             fd = -1;
/* socket stuff */
int                             sfd = -1;
struct sockaddr_in              myaddr;
struct sockaddr_in              remaddr;
int                             slen = sizeof(remaddr);
char                            *server = SERVER_IP;
/* Default capture device is usually /dev/video0 */
char                            *dev_name = DEVICE_NAME;
struct buffer                   buffers[REQUESTED_BUFFERS] = {0}; // hard coded number of buffers

unsigned int                    buffer_size = 65507; // hard coded max udp packet size


char user_buffer0 [65507];
char user_buffer1 [65507];
char user_buffer2 [65507];
char user_buffer3 [65507];

enum v4l2_buf_type              type;
fd_set                          fds;
struct timeval                  tv;
int                             r;
unsigned int                    i, n_buffers;
char                            out_name[256];
FILE                            *fout;


/* */
static void xioctl(int fh, int request, void *arg)
{
        int r;

        do 
        {
            //r = v4l2_ioctl(fh, request, arg);
            r = ioctl(fh, request, arg);
        } while (r == -1 && ((errno == EINTR) || (errno == EAGAIN)));

        if (r == -1) 
        {
            if (EINVAL == errno)
            fprintf(stderr, "%s does not support "
                                 "user pointer i/o\n", dev_name);
            fprintf(stderr, "error %d, %s\\n", errno, strerror(errno));
            exit(EXIT_FAILURE);
        }
}


void open_device(void)
{
        struct stat st;

        if (-1 == stat(dev_name, &st)) 
        {
                fprintf(stderr, "Cannot identify '%s': %d, %s\\n",
                         dev_name, errno, strerror(errno));
                exit(EXIT_FAILURE);
        }

        if (!S_ISCHR(st.st_mode)) 
        {
                fprintf(stderr, "%s is no devicen", dev_name);
                exit(EXIT_FAILURE);
        }

        
        fd = v4l2_open(dev_name, O_RDWR /* required */ | O_NONBLOCK, 0);
        
        if (fd < 0) 
        {
                fprintf(stderr, "Cannot open '%s': %d, %s\\n",
                         dev_name, errno, strerror(errno));
                exit(EXIT_FAILURE);
        }
}


void close_device(void)
{

}


void setup_format(void)
{
        CLEAR(fmt);
        fmt.type                = BUFFER_TYPE;
        fmt.fmt.pix.width       = FRAME_WIDTH;
        fmt.fmt.pix.height      = FRAME_HEIGHT;
        fmt.fmt.pix.pixelformat = VIDEO_FORMAT;
        fmt.fmt.pix.field       = VIDEO_FIELD_ORDER;

        xioctl(fd, VIDIOC_S_FMT, &fmt);

        if (fmt.fmt.pix.pixelformat != VIDEO_FORMAT) 
        {
                printf("Libv4l didn't accept H264 format. Can't proceed.\\n");
                exit(EXIT_FAILURE);
        }
        if ((fmt.fmt.pix.width != FRAME_WIDTH) || (fmt.fmt.pix.height != FRAME_HEIGHT))
        {
                printf("Warning: driver is sending image at %dx%d\\n",
                        fmt.fmt.pix.width, fmt.fmt.pix.height);
        }
}

void setup_framerate(void)
{
    int ret;
    CLEAR(parm);
    parm.type = BUFFER_TYPE;

    xioctl(fd, VIDIOC_G_PARM, &parm);

    printf("Current frame rate: %u/%u\n",
        parm.parm.capture.timeperframe.numerator,
        parm.parm.capture.timeperframe.denominator);

    parm.parm.capture.timeperframe.numerator = 1;
    parm.parm.capture.timeperframe.denominator = FRAMERATE;

    xioctl(fd, VIDIOC_S_PARM, &parm);

    printf("Frame rate set: %u/%u\n",
        parm.parm.capture.timeperframe.numerator,
        parm.parm.capture.timeperframe.denominator);
}


void setup_buffer_request(void)
{
        CLEAR(req);
        req.count       = REQUESTED_BUFFERS;
        req.type        = BUFFER_TYPE;
        req.memory      = MEMORY_TYPE;
        printf("about to make buffer request\n");
        xioctl(fd, VIDIOC_REQBUFS, &req);
}


void init_mmap(void)
{
    
    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) 
    {
        CLEAR(buf);

        buf.type        = BUFFER_TYPE;
        buf.memory      = MEMORY_TYPE;
        buf.index       = n_buffers;
        /* Query the buffer status */
        xioctl(fd, VIDIOC_QUERYBUF, &buf);
                
        /* Setup memory map */
        buffers[n_buffers].length = buf.length;
        buffers[n_buffers].start = v4l2_mmap(NULL, buf.length,
                            PROT_READ | PROT_WRITE, MAP_SHARED,
                            fd, buf.m.offset);

        if (MAP_FAILED == buffers[n_buffers].start) 
        {
            perror("mmap"); // Change this to fprintf
            exit(EXIT_FAILURE);
        }
    }
}

void init_userp(void)
{
    for (n_buffers = 0; n_buffers < req.count; ++n_buffers) 
    {
        CLEAR(buf);

        buf.type        = BUFFER_TYPE;
        buf.memory      = MEMORY_TYPE;
        buf.index       = n_buffers;
        /* Query the buffer status */
        xioctl(fd, VIDIOC_QUERYBUF, &buf);
        
        buffers[n_buffers].length = buf.length;
        
        // another malloc to get rid of
        //buffers[n_buffers].start = malloc(buffer_size);
        
        //buffers[n_buffers].start = malloc(buffer_size);
        //printf("malloc created a pointer @ %p\n", buffers[n_buffers].start);
        
        if(n_buffers ==1)
        {
        buffers[n_buffers].start = (void *)user_buffer0;
        printf("i created a pointer @ %p\n", buffers[n_buffers].start);
        }
        if(n_buffers ==2)
        {
        buffers[n_buffers].start = (void *)user_buffer1;
        printf("i created a pointer @ %p\n", buffers[n_buffers].start);
        }
        if(n_buffers ==3)
        {
        buffers[n_buffers].start = (void *)user_buffer2;
        printf("i created a pointer @ %p\n", buffers[n_buffers].start);
        }
        if(n_buffers ==4)
        {
        buffers[n_buffers].start = (void *)user_buffer3;
        
        printf("i created a pointer @ %p\n", buffers[n_buffers].start);
        }
         if (!buffers[n_buffers].start) {
                        fprintf(stderr, "Out of memory\n");
                        exit(EXIT_FAILURE);
                }
    }
}

/* */
void print_capabilities(struct v4l2_capability cap)
{
    printf("Available capabilities: 0x%08X\n", cap.capabilities);
    printf("Device capabilities: 0x%08X\n", cap.device_caps);
}


void check_capabilities(int fd)
{
        /* */
        struct v4l2_capability cap;
        
        CLEAR(cap);
        xioctl(fd, VIDIOC_QUERYCAP, &cap);
        print_capabilities(cap);
}

void setup_socket(void)
{
    /* Create a socket sfd is the socket file descriptor */
    if ((sfd = socket(AF_INET, SOCK_DGRAM, 0)) <0) 
    {
        perror("cannot create socket");
        exit(EXIT_FAILURE);
    }
    
    printf("created socket: descriptor=%d\n", sfd);
    
    /* bind it to all local addresses and pick any port number */

    memset((char *)&myaddr, 0, sizeof(myaddr));
    myaddr.sin_family = AF_INET;
    myaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    myaddr.sin_port = htons(0);
    
    if (bind(sfd, (struct sockaddr *)&myaddr, sizeof(myaddr)) < 0) 
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }
    
    /* now define remaddr, the address to whom we want to send messages */
    /* For convenience, the host address is expressed as a numeric IP address */
    /* that we will convert to a binary format via inet_aton */

    memset((char *) &remaddr, 0, sizeof(remaddr));
    remaddr.sin_family = AF_INET;
    remaddr.sin_port = htons(SERVER_PORT);
    if (inet_aton(server, &remaddr.sin_addr)==0) 
    {
        fprintf(stderr, "inet_aton() failed\n");
        exit(EXIT_FAILURE);
    }

}


int main(int argc, char **argv)
{
        //mlockall(MCL_CURRENT | MCL_FUTURE);
        /* Attempt to open the capture device */
        open_device();

        /* Attempt to set the image format and colorspace */
        setup_format();
        printf("past setup format\n");
        //setup_framerate();

        /* Setup doesn't request */
        setup_buffer_request();
        printf("past setup buffer request\n");
        /* Initialize memory map */
        //init_mmap();
        
        init_userp();
        printf("past userp\n");
        
        /* Queue buffer(s) */
        for (i = 0; i < n_buffers; ++i) {
            CLEAR(buf);
            buf.type   = BUFFER_TYPE;
            buf.memory = MEMORY_TYPE;
            buf.index  = i;
            buf.m.userptr = (unsigned long)buffers[i].start;
            buf.length = buffers[i].length;
            xioctl(fd, VIDIOC_QBUF, &buf);
        }
        
        printf("past queue buffers \n");

        type = BUFFER_TYPE;

        xioctl(fd, VIDIOC_STREAMON, &type);
        
        
        /* Open a file to save the stream to */
        //sprintf(out_name, "out.raw");
        //fout = fopen(out_name, "w");
        //if (!fout) {
        //    perror("Cannot open file"); //Change this to fprintf
        //    exit(EXIT_FAILURE);
        //}
        
        /* Create a socket to save the stream to */
        setup_socket();
        
        
        for (i = 0; i < FRAMES_TO_GRAB; i++) 
        {
                do 
                {
                        FD_ZERO(&fds);
                        FD_SET(fd, &fds);

                        /* Timeout. */
                        tv.tv_sec = BUFFER_FILL_TIMEOUT_SEC;
                        tv.tv_usec = BUFFER_FILL_TIMEOUT_USEC;
                        /* Waits until a buffer has been filled and */
                        /* can be dequeued with VIDIOC_DQBUF */
                        r = select(fd + 1, &fds, NULL, NULL, &tv);
                } while ((r == -1 && (errno = EINTR)));
                if (r == -1) {
                        perror("select"); //Change this to fprintf
                        return errno;
                }
                
                /* Dequeue a filled buffer from the outgoing queue */
                CLEAR(buf);
                buf.type   = BUFFER_TYPE;
                buf.memory = MEMORY_TYPE;
                
                xioctl(fd, VIDIOC_DQBUF, &buf);
                
                /* write to file */
                //fwrite(buffers[buf.index].start, buf.bytesused, 1, fout);

                /* write to socket */
                //printf("Sending packet %d to %s port %d size %u\n", i, server, SERVER_PORT, buf.bytesused);
                if (sendto(sfd, buffers[buf.index].start, buf.bytesused, 0, (struct sockaddr *)&remaddr, slen) == -1)
                {
                perror("sendto");
                }

                xioctl(fd, VIDIOC_QBUF, &buf);
        }
        
        /* Close device starts here */
        
        /* Turn off stream */
        type = BUFFER_TYPE;
        xioctl(fd, VIDIOC_STREAMOFF, &type);
        
        /* Tell the device that it can release memory */
        //req.count  = 0;
        //req.type   = BUFFER_TYPE;
        //req.memory = MEMORY_TYPE;
        //xioctl(fd, VIDIOC_REQBUFS, &req);
        
        /* Unmap the buffers */
        //for (i = 0; i < n_buffers; ++i)
        //{
        //    v4l2_munmap(buffers[i].start, buffers[i].length);
        //    v4l2_close(fd);
        //}
        //for (i = 0; i < n_buffers; ++i) {
        //                free(buffers[i].start);
                        v4l2_close(fd);
        //}   
        /* Close device stops here */
        
        /* Close the file */
        //fclose(fout);
        /* Close the socket */
        close(sfd);

        return 0;
}
