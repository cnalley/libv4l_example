
const dgram = require('dgram');
const socketPort = 1234
udpSocket = dgram.createSocket('udp4');

var http = require('http'),
    fs = require('fs'),
    // NEVER use a Sync function except at start-up!
    index = fs.readFileSync(__dirname + '/index.html');

// Send index.html to all requests
var app = http.createServer(function(req, res) {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(index);
});

// Socket.io server listens to our app
var io = require('socket.io').listen(app);

// Send current time to all connected clients
function sendTime() {
    io.emit('time', { time: new Date().toJSON() });
}

// Send current time every 10 secs
setInterval(sendTime, 10000);

// Every time I receive a UDP Message
udpSocket.on("message", function(msg,rinfo) {
    // create a buffer we will store to
    var buf = new Buffer(msg.length);
    // copy entire message into buffer
    msg.copy(buf, 0, 0, msg.length);
    io.emit('image', { image: true, buffer: buf.toString('base64') });
    console.log('image file is initialized');
});

// Emit welcome message on connection
io.on('connection', function(socket) {
    // Use socket to communicate with this particular client only, sending it it's own id
    socket.emit('welcome', { message: 'Welcome!', id: socket.id });
    socket.on('i am client', console.log);
});

udpSocket.bind(socketPort);
app.listen(3000);
